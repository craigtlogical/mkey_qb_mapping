﻿using DrivenBrands.MKey.Objects.Constants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DrivenBrands.MKey.Objects.Data
{
    public class QBVendorData
    {
        public DataTable GetQBVendors()
        {
            SqlCommand cmd = new SqlCommand("[sp_QBVendorGetAll]", MKDatabase.Instance.Connection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("QBVendors");
            da.Fill(dt);

            return dt;
        }

        public int SaveQBVendor(int ID, string Name)
        {
            System.Data.SqlClient.SqlParameter[] arrSqlParameter = new System.Data.SqlClient.SqlParameter[] {
                    new System.Data.SqlClient.SqlParameter("@ID", MKDatabase.CheckForNullValue(ID)),
                    new System.Data.SqlClient.SqlParameter("@Name", MKDatabase.CheckForNullValue(Name))};

            return ((int)(MKDatabase.ExecuteScalar(MKDatabase.Instance.Connection, "[sp_QBVendorSave]", arrSqlParameter)));
        }

        public void DeleteQBVendor(int ID)
        {
            System.Data.SqlClient.SqlParameter[] arrSqlParameter = new System.Data.SqlClient.SqlParameter[] {
                    new System.Data.SqlClient.SqlParameter("@ID", MKDatabase.CheckForNullValue(ID))};

            MKDatabase.ExecuteNonQuery(MKDatabase.Instance.Connection, "[sp_QBVendorDelete]", arrSqlParameter);
        }
        
    }
}


if exists (select * from sysobjects where type = 'P' and name = 'sp_QBVendorGetAll')
	begin
	    drop procedure dbo.sp_QBVendorGetAll
	end
go

create procedure dbo.sp_QBVendorGetAll
 /*******************************************************************************************************
 Logical Advantage, LLC
 www.logicaladvantage.com
 704-377-5066
 --------------------------------------------------------------------------------------------------------
 Stored Procedure:	sp_QBVendorGetAll
 
 Description:		Selects a QBVendor record for the given parameters
 
 --------------------------------------------------------------------------------------------------------
 Change Log:
 v1.0.0 - 4/3/2014  Original Release - Generated by LAAF
 ********************************************************************************************************
 To keep the Generator from overwriting this file add the word NOT between the center asterisks ( *NOT* )
 *** DO *NOT* GENERATE ***
 ********************************************************************************************************
 The result of this procedure call is used to populate the QBVendor Entity
 The Entity expects the following properties to be returned:
   ID < System.Int32 >
   Name < System.String >
 ********************************************************************************************************/

as
	select
		 ID
		,Name
	from dbo.QBVendor
	order by ID asc
		
go


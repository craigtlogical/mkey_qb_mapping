
if exists (select * from sysobjects where type = 'P' and name = 'sp_QBVendorDelete')
	begin
	    drop procedure dbo.sp_QBVendorDelete
	end
go

create procedure dbo.sp_QBVendorDelete
 /*******************************************************************************************************
 Logical Advantage, LLC
 www.logicaladvantage.com
 704-377-5066
 -------------------------------------------------------------------------------------------------------
 Stored Procedure:	sp_QBVendorDelete
 
 Description:		Deletes a QBVendor record for the given parameters
 
 --------------------------------------------------------------------------------------------------------
 Change Log:
 v1.0.0 - 4/3/2014  Original Release - Generated by LAAF
 ********************************************************************************************************
 To keep the Generator from overwriting this file add the word NOT between the center asterisks ( *NOT* )
 *** DO *NOT* GENERATE ***
 ********************************************************************************************************/
 @ID int

as
	delete
	from dbo.QBVendor
	where
		ID = @ID

go


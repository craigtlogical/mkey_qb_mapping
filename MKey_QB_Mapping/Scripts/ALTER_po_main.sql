SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


if not exists(select * from sys.columns 
            where Name = N'QBExportStatus' and Object_ID = Object_ID(N'po_main'))
begin
    alter table [dbo].[po_main] 
		add [QBExportStatus] int NULL 

	--update po_main set QBExportStatus = 9

end

GO

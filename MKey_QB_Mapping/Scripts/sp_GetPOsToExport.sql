
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'sp_GetPOsToExport') AND type = N'P')
DROP PROCEDURE sp_GetPOsToExport
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_GetPOsToExport]

as
begin

set nocount on

	select po_main.rec_date as [Received_Date],
		vendor.NAME as [Vendor_Name],	
		QBVendor.Name as [QB_Vendor_Name],
		po_main.WOInvoiceNum as [MKey_Invoice],
		isnull(po_main.WOEstimateNum, '') as [MKey_Estimate],
		coalesce(nullif(ltrim(rtrim(wo_item.Supplier_InvNum)), ''), po_main.inv_number) as [Vendor_PO_Num], 
		po_item.ITEM_ID as [Item_Id],
		po_item.descr as [Decription],
		isnull(wo_item.inv_num, '') as [Reference_Number],
		po_item.ReceivedQty as [Received_Qty],
		po_item.PRICE as [Item_Price],
		po_item.PRICE * po_item.ReceivedQty as [Total_Price],
		QBChartOfAccount.Name as [QB_Default_COA]
	from po_main
	join po_item on po_main.id = po_item.PO_ID
	join vendor on po_main.vendor_id = vendor.VENDOR_ID
	left join QBVendor on vendor.QBVendorID = QBVendor.ID
	left join QBChartOfAccount on vendor.QBChartOfAccountID = QBChartOfAccount.ID
	left outer join wo_item on po_item.Rec_Id = wo_item.rec_id
	where po_main.inv_number <> ''
		and isnull(po_main.QBExportStatus, 0) = 0
end

GO
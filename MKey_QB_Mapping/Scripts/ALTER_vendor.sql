SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


if not exists(select * from sys.columns 
            where Name = N'QBChartOfAccountID' and Object_ID = Object_ID(N'vendor'))
begin
    alter table [dbo].[vendor] 
		add [QBChartOfAccountID] int NULL 

end

GO

if not exists(select * from sys.columns 
            where Name = N'QBVendorID' and Object_ID = Object_ID(N'vendor'))
begin
    alter table [dbo].[vendor] 
		add [QBVendorID] int NULL 

end

GO
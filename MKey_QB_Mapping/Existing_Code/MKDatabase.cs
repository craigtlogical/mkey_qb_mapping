﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DrivenBrands.MKey.Objects.Constants
{
    public sealed class MKDatabase
    {
        private SqlConnection _Conn = new SqlConnection();
        private string ConnString = @"Data Source=(local); Initial Catalog=MPACT;User Id=mkeyuser;Password=C@tTattoo1;MultipleActiveResultSets=True;";
        static readonly MKDatabase instance = new MKDatabase();

        public static MKDatabase Instance
        {
            get
            {
                return instance;
            }
        }

        MKDatabase()
        {
            if (_Conn.State != ConnectionState.Open)
            {
                _Conn.ConnectionString = ConnString;
                _Conn.Open();
            }
        }

        public SqlConnection Connection
        {
            get
            {
                if (_Conn.State != ConnectionState.Open)
                {
                    _Conn.ConnectionString = ConnString;
                    _Conn.Open();
                } return _Conn;
            }
        }

        public static object ExecuteScalar(SqlConnection connection, string command, SqlParameter[] sqlParameters)
        {
            object retValue;

            SqlCommand sqlCommand = new SqlCommand(command, connection);
            sqlCommand = PrepareSqlCommand(sqlCommand, sqlParameters);
            retValue = sqlCommand.ExecuteScalar();

            return retValue;
        }

        public static void ExecuteNonQuery(SqlConnection connection, string command, SqlParameter[] sqlParameters)
        {

            SqlCommand sqlCommand = new SqlCommand(command, connection);

            sqlCommand = PrepareSqlCommand(sqlCommand, sqlParameters);

            sqlCommand.ExecuteNonQuery();
        }

        private static SqlCommand PrepareSqlCommand(SqlCommand sqlCommand, SqlParameter[] sqlParameters)
        {
            sqlCommand.CommandType = CommandType.StoredProcedure;

            foreach (SqlParameter parameter in sqlParameters)
            {
                sqlCommand.Parameters.Add(parameter);
            }

            return sqlCommand;
        }

        public static object CheckForNullValue(object value)
        {
            // Declare return value.
            object returnValue = System.DBNull.Value;

            // Ensure that the object is not null or DBNull.
            if (value != null && value != System.DBNull.Value)
            {
                // Get the string representation of the value's type.
                string valueTypeName = value.GetType().ToString();

                // Determine which type it is.
                switch (valueTypeName)
                {
                    case "System.Int32":
                        if ((int)value != int.MinValue)
                        {
                            returnValue = (int)value;
                        }
                        break;
                    case "System.DateTime":
                        if ((DateTime)value != DateTime.MinValue)
                        {
                            returnValue = (DateTime)value;
                        }
                        break;
                    case "System.Double":
                        if (!double.IsNaN((double)value) && (double)value != double.MinValue)
                        {
                            returnValue = (double)value;
                        }
                        break;
                    case "System.Int64":
                        if ((long)value != long.MinValue)
                        {
                            returnValue = (long)value;
                        }
                        break;
                    case "System.Int16":
                        if ((short)value != short.MinValue)
                        {
                            returnValue = (short)value;
                        }
                        break;
                    case "System.Guid":
                        if ((Guid)value != Guid.Empty)
                        {
                            returnValue = (Guid)value;
                        }
                        break;
                    case "System.Decimal":
                        if ((decimal)value != decimal.MinValue)
                        {
                            returnValue = (decimal)value;
                        }
                        break;
                    default:
                        returnValue = value;
                        break;
                }

            }
            // Return the value.
            return returnValue;
        }
    }
}

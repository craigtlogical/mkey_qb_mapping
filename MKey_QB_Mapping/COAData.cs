﻿using DrivenBrands.MKey.Objects.Constants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DrivenBrands.MKey.Objects.Data
{
    public class COAData
    {
        public DataTable GetCOAList()
        {
            SqlCommand cmd = new SqlCommand("[sp_QBChartOfAccountGetAll]", MKDatabase.Instance.Connection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("COA");
            da.Fill(dt);

            return dt;
        }

        public int SaveCOA(int ID, string Name)
        {
            System.Data.SqlClient.SqlParameter[] arrSqlParameter = new System.Data.SqlClient.SqlParameter[] {
                    new System.Data.SqlClient.SqlParameter("@ID", MKDatabase.CheckForNullValue(ID)),
                    new System.Data.SqlClient.SqlParameter("@Name", MKDatabase.CheckForNullValue(Name))};

            return ((int)(MKDatabase.ExecuteScalar(MKDatabase.Instance.Connection, "[sp_QBChartOfAccountSave]", arrSqlParameter)));

        }

        public void DeleteCOA(int ID)
        {
            System.Data.SqlClient.SqlParameter[] arrSqlParameter = new System.Data.SqlClient.SqlParameter[] {
                    new System.Data.SqlClient.SqlParameter("@ID", MKDatabase.CheckForNullValue(ID))};

            MKDatabase.ExecuteNonQuery(MKDatabase.Instance.Connection, "[sp_QBChartOfAccountDelete]", arrSqlParameter);
        }
    }
}

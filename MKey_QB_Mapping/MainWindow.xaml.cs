﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrivenBrands.MKey.Controls.Components
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCOA_Click(object sender, RoutedEventArgs e)
        {
            ucCOAEdit uc = new ucCOAEdit();
            CreateWindow(uc, "Chart of Accounts").ShowDialog();
        }
        private void btnQBVendors_Click(object sender, RoutedEventArgs e)
        {
            ucQBVendorEdit uc = new ucQBVendorEdit();
            CreateWindow(uc, "QuickBook Vendors").ShowDialog();
        }

        private void btnPOExport_Click(object sender, RoutedEventArgs e)
        {
            ucPOExport uc = new ucPOExport();
            CreateWindow(uc, "PO export").ShowDialog();
        }

        private static Window CreateWindow(UserControl uc, string title)
        {
            uc.Background = SystemColors.ControlLightBrush;
            Window w = new Window();
            w.Background = SystemColors.ControlLightBrush;
            w.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            w.WindowStyle = WindowStyle.SingleBorderWindow;
            w.Height = uc.Height + 5;
            w.Width = uc.Width + 5;
            w.ResizeMode = ResizeMode.NoResize;
            w.Content = uc;
            w.Title = title;

            return w;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrivenBrands.MKey.Controls.Components
{
    /// <summary>
    /// Interaction logic for ucPOExportHeader.xaml
    /// </summary>
    public partial class ucPOExportHeader : UserControl
    {


        public DataRow ItemSource
        {
            set
            {
                tbVendorName.Text = value["Vendor_Name"] as string;
                tbPONumber.Text = value["Vendor_PO_Num"] as string;
                tbDateReceived.Text = (value["Received_Date"]).ToString();
            }
        }

        public ucPOExportHeader()
        {
            InitializeComponent();
        }
    }
}

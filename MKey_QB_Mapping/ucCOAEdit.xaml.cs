﻿using DrivenBrands.MKey.Objects.Data;
using Microsoft.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DrivenBrands.MKey.Controls.Components
{
    /// <summary>
    /// Interaction logic for ucCOAMapping.xaml
    /// </summary>
    public partial class ucCOAEdit : UserControl
    {
        COAData _data = new COAData();
        private int _currentEditingID;
        private string _currentEditingName;

        public ucCOAEdit()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void LoadGrid()
        {
            DataTable data = _data.GetCOAList();
            dgMain.ItemsSource = data.DefaultView;
        }

        private void dgMain_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var uie = e.OriginalSource as UIElement;

            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                uie.MoveFocus(
                new TraversalRequest(
                FocusNavigationDirection.Next));
            }
        }

        private void btnDeleteVendor_Click(object sender, RoutedEventArgs e)
        {
            var drv = dgMain.SelectedItem as DataRowView;
            if (drv == null) return;

            int id = GetDataRowID(drv);

            MessageBoxResult result = MessageBox.Show(string.Format("Are you sure you want to delte COA {0}?", id), "Confirm Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                if (id > 0)
                {
                    try
                    {
                        _data.DeleteCOA(id);
                    }
                    catch (SqlException ex)
                    {
                        //TODO: Display error message
                        if (ex.Number == 547)
                        {
                            //Vendor in use
                            MessageBox.Show(string.Format("COA {0} is in use and can not be deleted.", id), "COA In Use", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }

                LoadGrid();
            }
        }

        private void dgMain_CellEditEnding(object sender, Microsoft.Windows.Controls.DataGridCellEditEndingEventArgs e)
        {
            var ctl = e.EditingElement as TextBox;
            var name = ctl.Text;

            if (string.IsNullOrEmpty(name))
            {
                ctl.Text = _currentEditingName;
            }
            else
            {
                int id = _data.SaveCOA(_currentEditingID, name);

                //TODO:Just update the one grid row
                LoadGrid();
            }
        }

        private void dgMain_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            var drv = e.Row.Item as DataRowView;
            _currentEditingID = GetDataRowID(drv);
            _currentEditingName = GetDataRowName(drv);
        }

        private int GetDataRowID(DataRowView drv)
        {
            return (drv.Row[0] == null || drv.Row[0] == DBNull.Value) ? int.MinValue : (int)drv.Row[0];
        }

        private string GetDataRowName(DataRowView drv)
        {
            return drv.Row[1] as string;
        }

    }
}

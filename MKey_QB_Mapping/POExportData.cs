﻿using DrivenBrands.MKey.Objects.Constants;
using System.Data;
using System.Data.SqlClient;

namespace DrivenBrands.MKey.Objects.Data
{
    public class POExportData
    {
        public DataTable GetPOExportList()
        {
            SqlCommand cmd = new SqlCommand("[sp_GetPOsToExport]", MKDatabase.Instance.Connection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("PO");
            da.Fill(dt);

            return dt;
        }
    }
}

﻿using DrivenBrands.MKey.Objects.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrivenBrands.MKey.Controls.Components
{
    /// <summary>
    /// Interaction logic for ucPOExport.xaml
    /// </summary>
    public partial class ucPOExport : UserControl
    {
        public ucPOExport()
        {
            InitializeComponent();

            LoadData();
        }

        private void LoadData()
        {
            POExportData data = new POExportData();
            DataTable dt = data.GetPOExportList();

            var groups = dt.AsEnumerable().GroupBy(r => r.Field<string>("Vendor_PO_Num"));

            foreach(var group in groups)
            {
                ucPOExportHeader header = new ucPOExportHeader();
                header.ItemSource = group.FirstOrDefault();
                spContent.Children.Add(header);

                DataTable detailTable = dt.Clone();
                foreach(var row in group.ToList())
                {
                    detailTable.ImportRow(row);
                }

                ucPOExportDetail detail = new ucPOExportDetail();
                detail.ItemSource = detailTable.DefaultView;
                spContent.Children.Add(detail);

            }
        }
    }
}

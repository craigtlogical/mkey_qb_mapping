﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrivenBrands.MKey.Controls.Components
{
    /// <summary>
    /// Interaction logic for ucPODetail.xaml
    /// </summary>
    public partial class ucPOExportDetail : UserControl
    {
        public System.Collections.IEnumerable ItemSource 
        { 
            set
            {
                dgMain.ItemsSource = value;
            }
        }

        public ucPOExportDetail()
        {
            InitializeComponent();
        }
    }
}
